<meta charset="utf-8">
<title>
    Virtual Learning System
</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="{{ asset('/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('/bootstrap/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('/bootstrap/css/quickadmin-layout.css') }}">
<link rel="stylesheet" href="{{ asset('/bootstrap/css/quickadmin-theme-default.css') }}">
<link rel="icon" href="{{ asset('img/favicon-32x32.png') }}" />
<script src="{{ asset('/bootstrap/js/jquery.min.js') }}"></script>
<script src="{{ asset('/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- <link rel="stylesheet" href="{{ asset('/css/style.css') }}"> -->
<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<!------ Include the above in your HEAD tag ---------->
<!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script> -->


<style>
    html,
    body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        /* height: 100vh; */
        margin: 0;
    }

    body {
        position: relative;
        /* padding-bottom: 112px; */
        min-height: 100vh;
        background-color: #f1f1f1;
    }

    footer {
        position: absolute;
        bottom: 0;
        width: 100%;
    }

    .main {
        /* width: 90vw; */
        margin: 0 auto;
        padding: 15px 0px;
        background-color: white;
    }
    .break {
        margin-bottom: 1.5rem !important;
    }

    .navbar {
        margin-bottom: 1.5rem !important;
        ;
    }

    #sidebar {
        background-color: #343a40;
    }

    a {
        color: inherit;
        text-decoration: none
    }

    .hr {
        height: 2px;
        margin: 60px 0 50px 0;
        background: rgba(255, 255, 255, .2)
    }

    .foot {
        text-align: center
    }

    .modal-body {
        padding: 0;
    }

    ::placeholder {
        color: #b3b3b3
    }

    .label {
        font-weight: 100;
        color: #007bff;
        /* background-color: transparent; */
        font-size: 1rem;
    }

    /* Tabs*/
    section {
        padding-top: 60px;
    }

    section .section-title {
        text-align: center;
        color: #007b5e;
        margin-bottom: 50px;
        text-transform: uppercase;
    }

    #tabs {
        background: #01133f;
        /* color: #eee; */
    }

    #tabs h6.section-title {
        color: #eee;
    }

    #tabs .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        color: #f3f3f3;
        background-color: transparent;
        border-color: transparent transparent #f3f3f3;
        border-bottom: 4px solid !important;
        font-size: 15px;
        font-weight: bold;
    }

    #tabs .nav-tabs .nav-link {
        border: 1px solid transparent;
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem;
        color: #eee;
        /* font-size: 20px; */
    }

    /* .carousel {

        height: 400px;

    }

    .carousel-inner,
    .item,

    img {

        height: 400px;

        object-fit: contain;

    } */
    .col-xs-12 {
        width: 100%;
    }

    .disabled {
        pointer-events: none;
        color: grey;
    }

    .collapsible:after {
        content: '\002B';
        font-weight: bold;
        float: right;
        margin-left: 5px;
        padding-top: 12px;
    }

    /* .active:after {
        content: "\2212";
    } */

    .card-body-img {
        padding: 0.25rem;
    }

    .myaccordion {
        box-shadow: 0 0 1px rgba(0, 0, 0, 0.1);
    }

    .myaccordion .card,
    .myaccordion .card:last-child .card-header {
        border: none;
    }

    .myaccordion .card-header {
        border-bottom-color: #EDEFF0;
        background: transparent;
    }

    .myaccordion .fa-stack {
        font-size: 18px;
    }

    .myaccordion .btn {
        width: 100%;
        font-weight: bold;
        color: #004987;
        padding: 0;
    }

    .myaccordion .btn-link:hover,
    .myaccordion .btn-link:focus {
        text-decoration: none;
    }

    .myaccordion li+li {
        margin-top: 10px;
    }

    .card {
        border-radius: 0;
    }

    .login-box {
        border-radius: 10px;
        box-shadow: 5px 5px 10px #dfdfdf, -5px -5px 10px #eee;
        margin: 10% auto;
    }


    .avatar {
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
        top: -75px;
        width: 140px;
        height: 95px;
        border-radius: 50%;
        z-index: 9;
        padding: 25px;
    }

    .avatar img {
        width: 100%;
    }
</style>