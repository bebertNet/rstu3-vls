<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="/">RSTU 3 Virtual Learning System</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">

        <span class="navbar-toggler-icon"></span>

    </button>
    <!-- Links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/home">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Training
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach($courses as $key=>$course)
                    @if(Auth::check())
                    @if(Auth::user()->Course == $course->id || Auth::user()->Course == "admin")
                    <a class="dropdown-item" href="/courses/{{$course->short}}">
                        {{ $course->Name }}
                    </a>
                    @else
                    <a class="dropdown-item disabled" href="/courses/{{$course->short}}">
                        {{ $course->Name }}
                    </a>
                    @endif
                    @endif
                    @endforeach
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/about">About Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/contactUs">Contact Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/fileGallery">File Gallery</a>
            </li>
        </ul>
        
        <ul class="nav navbar-nav ml-auto">
            <!-- <li class="nav-item">
            <a class="nav-link" href="#"><span class="fas fa-user"></span> Sign Up</a>
        </li> -->
            @guest  
            <li class="nav-item">
                <!-- <a class="nav-link" id="pop" data-toggle="modal" data-target="#modalPassword">
                    <span class="fas fa-sign-in-alt"></span> Login</a> -->
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->UserName }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                    @if (Auth::user()->Course == "admin")
                    <a class="dropdown-item" href="/admin" onclick="event.preventDefault();
                                                     document.getElementById('admin-form').submit();">
                        Admin
                    </a>
                    <form id="admin-form" action="/admin" method="" style="display: none;">
                    </form>
                    @endif
                </div>
                
                
            </li>
            @endguest
        </ul>
    </div>

    <!-- <ul class="nav navbar-nav flex-row justify-content-between ml-auto">
        <li class="nav-item order-2 order-md-1"><a href="#" class="nav-link" title="settings"><i class="fa fa-cog fa-fw fa-lg"></i></a></li>
        <li class="dropdown order-1">
            <button type="button" id="dropdownMenu1" data-toggle="dropdown" class="btn btn-outline-secondary dropdown-toggle">Login <span class="caret"></span></button>
            <ul class="dropdown-menu dropdown-menu-right mt-2">
                <li class="px-3 py-2">
                    <form class="form" role="form">
                        <div class="form-group">
                            <input id="emailInput" placeholder="Email" class="form-control form-control-sm" type="text" required="">
                        </div>
                        <div class="form-group">
                            <input id="passwordInput" placeholder="Password" class="form-control form-control-sm" type="text" required="">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Login</button>
                        </div>
                        <div class="form-group text-center">
                            <small><a href="#" data-toggle="modal" data-target="#modalPassword">Forgot password?</a></small>
                        </div>
                    </form>
                </li>
            </ul>
        </li>
    </ul> -->
</nav>