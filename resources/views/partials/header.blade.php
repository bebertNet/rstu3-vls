<div class="container-fluid">
    <img src="{{ asset('/img/banner-RSTU3.png') }}" class="img-fluid" alt="banner">
    <hr>
    @include('partials/nav')
    <div id="modalPassword" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <!-- dito ilagay ang error -->

                    <div class="card">

                        <div class="login-box">
                            <div class="login-snip">
                                <input id="tab-1" type="radio" name="tab" class="sign-in" checked>
                                <label for="tab-1" class="tab">
                                    Login
                                </label>
                                <input id="tab-2" type="radio" name="tab" class="sign-up">
                                <label for="tab-2" class="tab">
                                </label>

                                <div class="login-space">
                                    <div class="login">
                                        <div class="alert alert-danger print-error-msg" style="display:none">
                                            <ul></ul>
                                        </div>

                                        <!-- must command in cadence command -->
                                        <form>
                                            {{ csrf_field() }}
                                            <div class="group"> <label for="user" class="label">Username</label> <input name="user" id="user" type="text" class="input" placeholder="Enter your username"> </div>
                                            <div class="group"> <label for="pass" class="label">Password</label> <input name="pass" id="pass" type="password" class="input" data-type="password" placeholder="Enter your password"> </div>
                                            <div class="group"> <input id="check" type="checkbox" class="check" checked> <label for="check"><span class="icon"></span> Keep me Signed in</label> </div>
                                            <div class="group"> <input type="submit" class="button" name="submit" id="submit" value="Sign In"> </div>
                                            <div class="hr"></div>
                                            <div class="foot">
                                                <a href="#">
                                                    Forgot Password?
                                                </a>
                                            </div>
                                        </form>

                                    </div>
                                    <!-- <div class="sign-up-form">
                                        <form action="/create/user" method="POST">
                                            {{@csrf_field()}}
                                            <div class="group"> <label for="fname" class="label">First Name</label> <input name="fname" id="fname" type="text" class="input" placeholder="Enter your First Name"> </div>
                                            @error('fname')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                            <div class="group"> <label for="mname" class="label">Middle Name</label> <input name="mname" id="mname" type="text" class="input" placeholder="Enter your Middle Name"> </div>
                                            <div class="group"> <label for="lname" class="label">Last Name</label> <input name="lname" id="lname" type="text" class="input" placeholder="Enter your Last Name"> </div>
                                            <div class="group"> <label for="email" class="label">Email</label> <input name="email" id="email" type="text" class="input" placeholder="Enter your Email"> </div>
                                            <div class="group"> <label for="user" class="label">Username</label> <input name="username" id="user" type="text" class="input" placeholder="Create your Username"> </div>
                                            @error('username')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                            <div class="group"> <label for="pass" class="label">Password</label> <input id="pass" type="password" class="input" data-type="password" placeholder="Create your password"> </div>
                                            <div class="group"> <label for="pass" class="label">Repeat Password</label> <input id="pass" type="password" class="input" data-type="password" placeholder="Repeat your password"> </div>
                                            <div class="group"> <input type="submit" class="button" value="Sign Up"> </div>
                                            <div class="hr"></div>
                                            <div class="foot"> <label for="tab-1">Already Member?</label> </div>
                                        </form>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>