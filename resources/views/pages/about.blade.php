@extends('layouts.app')
@section('content')

<div class="container-fluid">
    <h2> HISTORY OF RSTU 3</h2>
    <p class="text-justify">
        The Philippine National Police Training Service was created as an interim unit to accelerate professionalism among PNP personnel, the Center for Specialized Courses and the Reformatory School were placed under it. On August 10, 2005, the organizational structure of PNPTS was amended to include four (4) functional schools: the School for Specialized Courses; the School for Values and Leadership; the School for Sports and Skills Development and the School for Field Training and Technical Services.</p>
    <p class="text-justify">
        The NAPOLCOM formally approved the establishment of the PNP Training Service by virtue of NAPOLCOM Resolution Nr. 2005-388 (Approving the Establishment of PNP Training Service) dated November 9, 2005 with the main task of conducting specialized training and PNP in-service training programs. The PNPTS became a Natioanal Administrative Unit (NASU) of the PNP under the functional supervision of the Directorate for Human Resource and Doctrine Deevelopment (DHRDD). It serves as the in-service training facility of the PNP which implements human resource training programs particularly specialized trainings.
    </p>
    <p class="text-justify"> PSSUPT BENJMIN A BELARMINO,JR., the 1st PNPTS Director temporarily held office at the PNP Maritime Group. Under its 2nd Director, then PSSUPT JOSE ARN M DELOS SANTOS, the PNPTS transferred to the former Detective School Building. </p>

    <p class="text-justify"> To enhance its humand resource requirements, the Non-Uniformed Personnel formerly detailed with the Philippine Drug Enforcement Agency (PDEA) were assigned to PNPTS.</p>

    <p class="text-justify"> Further, PNP Memorundum Circular No. 2006-010 dated June 14, 2006 was issued requiring those personnel who came from UN peacekeeping missions to beef-up the personnel strength of the PNPTS.</p>

    <p class="text-justify">On October 7, 2009, the Chief PNP issued a Memorundum Circular No. 2009-018 which provided the implementing guidelines and procedures governing the functions of the PNP Training Service with the absorption and exercise of administrative and operational control and supervision over all existing training units in the PNP as well as the Regional Special Training Units (RSTUs).
    </p>
    <p class="text-justify">On August 6, 2011, the Center for Law Enforcement Studies (CLES) Building was formally turned-over by the PSMBFI. A month later, it became the home of the PNPTS Headquarters.</p>
</div>

@stop