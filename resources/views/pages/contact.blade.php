@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<h2> Contact Us </h2>
	<div class="table-responsive">
		<table class="table table-stripped table-bordered table-hover">
			<tr>
				<td><b> Address :</b></td>
				<td> <a target="_blank" href="https://www.google.com/maps/dir/15.1852473,120.5394227/7372+Gil+Puyat+Ave,+Clark+Freeport,+Mabalacat,+Pampanga/@15.1865509,120.5357255,17z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x3396ed4e34752435:0x8da5e5b04493bcaf!2m2!1d120.5363826!2d15.1870458"> Building 7372, Gil Puyat Avenue, Clark Freeport Zone, Pampanga </a>
				</td>
			</tr>
			<tr>
				<td><b> Email :</b></td>
				<td>rstu3.ts@pnp.gov.ph</td>
			</tr>
			<tr>
				<td><b> Facebook:</b></td>
				<td><a target="_blank" href="https://www.facebook.com/rupert.dixon.1">www.facebook.com/rstuTres</a></td>
			</tr>

		</table>
	</div>
</div>

@stop