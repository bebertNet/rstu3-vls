@extends('layouts.app')
@section('content')
<script>
    // $(function() {
    //     $("#button").on("click", function() {
    //         $("#p").removeClass("disabled");
    //     });
    // });
    var counter = 0;

    function EditSelectedOptionName(total, count, id) {
        counter = counter + parseInt(count);
        console.log(total);
        console.log(counter);
        console.log("#" + id);
        if (total == counter) {
            $("#" + id).removeClass("disabled");
            counter = 0;
        }

    }
</script>
<div class="container-fluid">
    <h3 class="text-center">Patrol Officers Basic Course (POBC)</h3>
    <!-- Tabs -->
    <section id="tabs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent tab-content">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="myaccordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingPatOne">
                                        <h2 class="mb-0">
                                            <span class="label collapsed" data-toggle="collapse" data-target="#collapsePatOne" aria-expanded="false" aria-controls="collapsePatOne">
                                                Introduction
                                            </span>
                                        </h2>
                                    </div>

                                    <div id="collapsePatOne" class="collapse show" aria-labelledby="headingPatOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>Information will be provided by the TS Intern
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingPatTwo">
                                        <h2 class="mb-0">
                                            <span class="label collapsed" data-toggle="collapse" data-target="#collapsePatTwo" aria-expanded="false" aria-controls="collapsePatTwo">
                                                PHASE INFORMATION
                                            </span>
                                        </h2>
                                    </div>
                                    <div id="collapsePatTwo" class="collapse" aria-labelledby="headingPatTwo" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                Information will be provided by the TS Intern
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingPatThree">
                                        <h2 class="mb-0">
                                            <span class="label collapsed" data-toggle="collapse" data-target="#collapsePatThree" aria-expanded="false" aria-controls="collapsePatThree">
                                                TRAINING GOAL
                                            </span>
                                        </h2>
                                    </div>
                                    <div id="collapsePatThree" class="collapse" aria-labelledby="headingPatThree" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                Information will be provided by the TS Intern
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingPatFive">
                                        <h2 class="mb-0">
                                            <span class="label collapsed" data-toggle="collapse" data-target="#collapsePatFIve" aria-expanded="false" aria-controls="collapsePatFIve">
                                                TRAINING OBJECTIVES
                                            </span>
                                        </h2>
                                    </div>
                                    <div id="collapsePatFIve" class="collapse" aria-labelledby="headingPatFive" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                Information will be provided by the TS Intern
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingPatFour">
                                        <h2 class="mb-0">
                                            <span class="label collapsed" data-toggle="collapse" data-target="#collapsePatFour" aria-expanded="false" aria-controls="collapsePatFour">
                                                TRAINING Audience
                                            </span>
                                        </h2>
                                    </div>
                                    <div id="collapsePatFour" class="collapse" aria-labelledby="headingPatFour" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                Information will be provided by the TS Intern
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingPatSix">
                                        <h2 class="mb-0">
                                            <span class="label collapsed" data-toggle="collapse" data-target="#collapsePatSix" aria-expanded="false" aria-controls="collapsePatSix">
                                                DURATION
                                            </span>
                                        </h2>
                                    </div>
                                    <div id="collapsePatSix" class="collapse" aria-labelledby="headingPatSix" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                Information will be provided by the TS Intern
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingPatSeven">
                                        <h2 class="mb-0">
                                            <span class="label collapsed" data-toggle="collapse" data-target="#collapsePatSeven" aria-expanded="false" aria-controls="collapsePatSeven">
                                                LEARNING AIDS
                                            </span>
                                        </h2>
                                    </div>
                                    <div id="collapsePatSeven" class="collapse" aria-labelledby="headingPatSeven" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>
                                                Information will be provided by the TS Intern
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @foreach($Modules as $module)
                                <div class="card">
                                    <div class="card-header" id="heading{{ $module->Id }}">
                                        <h2 class="mb-0">
                                            @if($loop->first)
                                            <span class="label collapsed" data-toggle="collapse" data-target="#collapse{{ $module->Id }}" aria-expanded="false" aria-controls="collapse{{ $module->Id }}">
                                                {{ $module->Name }} {{ $module->Description }}
                                            </span>
                                            @else
                                            <span class="label collapsed" data-toggle="collapse" data-target="#collapse{{ $module->Id }}" aria-expanded="false" aria-controls="collapse{{ $module->Id }}" id="card{{ $module->Id }}">
                                            {{ $module->Name }} {{ $module->Description }}
                                            </span>
                                            @endif

                                        </h2>
                                    </div>
                                    <div id="collapse{{ $module->Id }}" class="collapse" aria-labelledby="heading{{ $module->Id }}" data-parent="#accordionExample">
                                        <div class="card-body" id="child{{ $module->Id }}">
                                            @foreach($SubModules as $submodule)
                                            @if($submodule->moduleId == $module->Id)
                                            <div class="card">
                                                <div class="card-header" id="headingSub{{ $submodule->id }}">
                                                    <h2 class="mb-0">
                                                        @if($loop->first)
                                                        <span class="label collapsed" data-toggle="collapse" data-target="#collapseSub{{ $submodule->id }}" aria-expanded="false" aria-controls="collapseSub{{ $submodule->id }}">
                                                            {{ $submodule->submoduleName }}
                                                        </span>
                                                        @else
                                                        <span class="label collapsed" data-toggle="collapse" data-target="#collapseSub{{ $submodule->id }}" aria-expanded="false" aria-controls="collapseSub{{ $submodule->id }}" id="card{{ $submodule->id }}">
                                                            {{ $submodule->submoduleName }}
                                                        </span>
                                                        @endif

                                                    </h2>
                                                </div>
                                                <div id="collapseSub{{ $submodule->id }}" class="collapse" aria-labelledby="headingSub{{ $submodule->id }}" data-parent="#child{{ $module->Id }}">
                                                    <div class="card-body card-body-img">
                                                        <div id="carousel{{ $submodule->id }}" class="carousel slide" data-ride="carousel" data-interval="false">
                                                            <ol class="carousel-indicators">
                                                                @foreach($PobcImages as $key=>$image)
                                                                @if($image->submodule_id == $submodule->id)
                                                                @if($loop->first)
                                                                <li data-target="#carousel{{ $submodule->id }}" data-slide-to="$key" class="active"></li>
                                                                @else
                                                                <li data-target="#carousel{{ $submodule->id }}" data-slide-to="$key"></li>
                                                                @endif
                                                                @endif
                                                                @endforeach
                                                            </ol>
                                                            <div class="carousel-inner">
                                                                <?php $count = 0; ?>
                                                                @foreach($PobcImages as $key=>$image)
                                                                @if($image->submodule_id == $submodule->id)
                                                                <?php $count++; ?>
                                                                @if($loop->first)
                                                                <div class="carousel-item active">
                                                                    <img class="d-block w-100" src="{{ url( $image->path ) }}" alt="{{ url( $image->path ) }}">
                                                                </div>
                                                                @else
                                                                <div class="carousel-item">
                                                                    <img class="d-block w-100" src="{{ url( $image->path ) }}" alt="{{ url( $image->path ) }}">
                                                                </div>
                                                                @endif
                                                                @unset($PobcImages[$key])
                                                                @endif
                                                                @endforeach
                                                            </div>
                                                            <a class="carousel-control-prev" href="#carousel{{ $submodule->id }}" role="button" data-slide="prev">
                                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                                <span class="sr-only">Previous</span>
                                                            </a>
                                                            <a class="carousel-control-next" href="#carousel{{ $submodule->id }}" role="button" id="button" onclick="EditSelectedOptionName('{{ $count }}','1', 'card{{ $submodule->id + 1 }}')" data-slide="next">
                                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                                <span class="sr-only">Next</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- ./Tabs -->
</div>
@stop