<div class="myaccordion" id="accordionInv">
	<div class="card">
		<div class="card-header" id="headingOne">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
					Introduction

				</span>
			</h2>
		</div>

		<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionInv">
			<div class="card-body">
				<p>This Police Trainee’s Guide is a compilation of all the modules to be discussed under
					Investigation Phase of the Field Training Program. This guide comprises selected
					penal laws being enforced by the PNP and selected rules on criminal procedures as
					well as the procedures to be undertaken by the new police officers in the conduct
					of police investigation. Apart from the main topics, this guide is also complemented
					with the knowledge on how to accomplish the blotter properly and the introduction of
					Incident Report Form in the evolving world of policing. Likewise, report writing, making
					of affidavit of arrest, actual experience through observation in different court hearings
					and fusion of Barangay Justice System was added to complete this package in basic
					investigation. </p>
				<p>
					On the other hand, PTs will be exposed to the different scenarios through simulation
					exercise, table top exercise and even actual experience in the field in order to reinforce
					their knowledge and develop their skills. Through this way, we will produce better PTs
					who are more effective, credible and capable police officers in the field of investigation.</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTwo">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					PHASE INFORMATION
				</span>
			</h2>
		</div>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionInv">
			<div class="card-body">
				<p>
					The next eight (8) weeks in the Field Training Program is devoted to the Investigation
					Phase composed of eight (8) modules.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingThree">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
					TRAINING GOAL
				</span>
			</h2>
		</div>
		<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionInv">
			<div class="card-body">
				<p>
					The goal of this phase is to introduce to the PTs to the basic and fundamental knowledge
					in investigation as one of the law enforcement functions in the Philippine National Police.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingFive">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
					TRAINING OBJECTIVES
				</span>
			</h2>
		</div>
		<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionInv">
			<div class="card-body">
				<p>
					At the end of this phase, the PTs must be able to:
					<br>
					1. Define their job as the first person to arrive at the crime scene in order to protect,
					preserve and secure the vital evidence necessary in the investigation of case. <br>
					2. Distinguish what law is applicable for every violation committed and penalized
					under the Revised Penal Code and Special Laws as the basic foundation to
					effectively perform their job as law enforcers in the future <br>
					3. Familiarize the PTs on the proper use of Police Blotter and Incident Report from
					as an instrument of recording all administrative and operational aspects of the
					police station. <br>
					4. Make a complete and accurate spot report <br>
					5. Prepare their own affidavit of arrest as an instrument to support the filing of
					case in court.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingFour">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
					TRAINING Audience
				</span>
			</h2>
		</div>
		<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionInv">
			<div class="card-body">
				<p>
					The target audience for this phase are the Police Trainees (PTs) who undergone
					and completed the six (6) months Public Safety Basic Course Recruit (PSBRC).
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingSix">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
					DURATION
				</span>
			</h2>
		</div>
		<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionInv">
			<div class="card-body">
				<p>
					This Phase shall run for a total of two (2) months equivalent to eight (8) weeks with
					48 days excluding the eight (8) Saturdays with a total of 384 training hours.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingSeven">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
					LEARNING AIDS
				</span>
			</h2>
		</div>
		<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionInv">
			<div class="card-body">
				<p>
					The following are the learning aids required for the duration of the phase:
					<ul>
						<li> Multi-Media Projector </li>
						<li> Multi-Media Pointer </li>
						<li> Laptop Computer </li>
						<li> Projector Screen </li>
						<li> Voice Recorder</li>
						<li> Flip Charts and markers </li>
						<li> Manila Paper </li>
						<li> Extra Sheets of Paper </li>
						<li> Investigation Section Organizational Structure </li>
						<li> Area of Responsibility (AOR) Operational Map </li>
					</ul>
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingEight">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
					Module 1 ( Orientation and Familiarization.Duties, responsibilities of an Investigator)
				</span>
			</h2>
		</div>
		<div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionInv">
			<div class="card-body">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($InvMod1Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselExampleIndicators" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselExampleIndicators" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>

					<div class="carousel-inner">
						@foreach($InvMod1Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
				<!-- <iframe src="https://onedrive.live.com/embed?cid=92097F8E58AE4B7D&resid=92097F8E58AE4B7D%21614&authkey=ALkJSFOc2FODSPE&em=2" width="800" height="400" frameborder="0" scrolling="no"></iframe> -->
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingNine">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
					Module 2 ( Police Blotter and Incident Report Form (IRF))
				</span>
			</h2>
		</div>
		<div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionInv">
			<div class="card-body">
				<div id="carouselMod2" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($InvMod2Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselMod2" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselMod2" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($InvMod2Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselMod2" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselMod2" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
				<!-- <video controls autoplay style="width:100%;">
					<source src="{{ asset('/storage/docs/Slides.mp4') }}" type="video/mp4" />
				</video> -->
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTen">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
					Module 3 ( Police Report Writing)
				</span>
			</h2>
		</div>
		<div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionInv">
			<div class="card-body">
				<div id="carouselMod3" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($InvMod3Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselMod3" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselMod3" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($InvMod3Images as $key=>$image)
							@if($loop->first)
							<div class="carousel-item active">
								<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
							</div>
							@else
							<div class="carousel-item">
								<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
							</div>
							@endif

							@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselMod3" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselMod3" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingEl">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseEl" aria-expanded="false" aria-controls="collapseEl">
					Module 4 ( Basic knowledge on laws under the Revised Penal Code, Special Laws and Rules on Criminal Procedures)
				</span>
			</h2>
		</div>
		<div id="collapseEl" class="collapse" aria-labelledby="headingEl" data-parent="#accordionInv">
			<div class="card-body">
				<div id="carouselInvMod4" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($mod4Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselInvMod4" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselInvMod4" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($mod4Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselInvMod4" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselInvMod4" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTwe">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTwe" aria-expanded="false" aria-controls="collapseTwe">
					Module 5 (Basic roles of first responder in crime scene investigation procedures)
				</span>
			</h2>
		</div>
		<div id="collapseTwe" class="collapse" aria-labelledby="headingTwe" data-parent="#accordionInv">
			<div class="card-body">
				<div id="carouselInvMod5" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($mod5Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselMod5" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselMod5" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($mod5Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselInvMod5" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselInvMod5" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingThir">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseThir" aria-expanded="false" aria-controls="collapseThir">
					Module 6 (Affidavit of Making Arrest)
				</span>
			</h2>
		</div>
		<div id="collapseThir" class="collapse" aria-labelledby="headingThir" data-parent="#accordionInv">
			<div class="card-body">
				<div id="carouselMod6" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($InvMod6Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselMod6" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselMod6" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($InvMod6Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselMod6" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselMod6" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="heading14">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
					Module 7 (Court Decorum and Observation)
				</span>
			</h2>
		</div>
		<div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordionInv">
			<div class="card-body">
				<div id="carouselMod7" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($InvMod7Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselMod7" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselMod7" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($InvMod7Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselMod7" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselMod7" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingAssessment">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseAssessment" aria-expanded="false" aria-controls="collapseAssessment">
					<a target="_blank" href="http://vls-exam.rstu3.com">Take Exam</a>
				</span>
			</h2>
		</div>

	</div>
</div>