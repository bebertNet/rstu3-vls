<div class="myaccordion" id="accordionTraf">
	<div class="card">
		<div class="card-header" id="headingTrafOne">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafOne" aria-expanded="false" aria-controls="collapseTrafOne">
					Introduction
				</span>
			</h2>
		</div>

		<div id="collapseTrafOne" class="collapse show" aria-labelledby="headingTrafOne" data-parent="#accordionTraf">
			<div class="card-body">
				<p class="justify-content">This Field Training Officer’s Guide is a simplified compilation of all knowledge
					needed by Police Trainees (PTs) as they undergo the Traffic Phase of the Field
					Training Program (FTP). This is designed to complement academic instructions
					through practical experiences in the field which will ultimately prepare them for the
					next step towards becoming police officers.
					The Traffic Phase deals with the concepts of traffic, its goals, objectives, and
					processes. It also deals with traffic safety education, basic and common traffic
					enforcer actions. It further identifies collective impression, on traffic when they join the
					organization as police officers.
					The PTs will gradually acquire knowledge and skills necessary to perform as
					able police officers. The exact point in training (time) at which the transition is made
					from one step to the next varies from each trainee. The goal is to develop the PTs to
					become effective, credible and experienced police officers with the highest caliber of
					public service.
					Traffic is a complex system which targets the movement or flow of vehicles,
					pedestrians, and goods from point of origin to destination. The Traffic Phase will equip
					the PTs with knowledge, understanding and skills to address any setbacks and provide
					innovative traffic management services for a more coordinated, better and safer
					community. This also gives an overview of the actual performance of police officers in
					the field focusing on the standard traffic management procedures and processes in
					the locality.
					This Guide emphasizes on the observable learning that needs to be taught and
					imparted to the PTs by the Field Training Officers (FTOs). It also concentrates on
					the standard and systematic application of the learning process. Various teaching
					methods will be applied like demonstrations, dramatizations, video presentations,
					practical exercises, and field immersions for a more holistic learning process to help
					develop the skills of PTs in the field of traffic.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafTwo">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafTwo" aria-expanded="false" aria-controls="collapseTrafTwo">
					PHASE INFORMATION
				</span>
			</h2>
		</div>
		<div id="collapseTrafTwo" class="collapse" aria-labelledby="headingTrafTwo" data-parent="#accordionTraf">
			<div class="card-body">
				<p class="justify-content">
					The Traffic Phase will be conducted for a period of four (4) weeks with eight (8)
					modules. It was designed for FTOs to effectively teach the police trainees.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafThree">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafThree" aria-expanded="false" aria-controls="collapseTrafThree">
					TRAINING GOAL
				</span>
			</h2>
		</div>
		<div id="collapseTrafThree" class="collapse" aria-labelledby="headingTrafThree" data-parent="#accordionTraf">
			<div class="card-body">
				<p class="justify-content">
					This Phase aims to introduce to the PTs the basic and fundamental knowledge in
					traffic as one of the law enforcement functions of the Philippine National Police.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafFive">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafFIve" aria-expanded="false" aria-controls="collapseTrafFIve">
					TRAINING OBJECTIVES
				</span>
			</h2>
		</div>
		<div id="collapseTrafFIve" class="collapse" aria-labelledby="headingTrafFive" data-parent="#accordionTraf">
			<div class="card-body">
				<p>
					At the end of this Phase, the PTs will be able to:
					<br> &MediumSpace;
					1. Define traffic and its processes; <br> &MediumSpace;
					2. Appreciate and value the traffic management processes to further promote <br> &MediumSpace;
					awareness as part of their professional and personal growth; and <br> &MediumSpace;
					3. Acquire the basic and fundamental skills on traffic procedures.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafFour">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafFour" aria-expanded="false" aria-controls="collapseTrafFour">
					TRAINING Audience
				</span>
			</h2>
		</div>
		<div id="collapseTrafFour" class="collapse" aria-labelledby="headingTrafFour" data-parent="#accordionTraf">
			<div class="card-body">
				<p>
					The target audience for this Phase are PTs who completed the six (6) months
					Public Safety Basic Recruit Course (PSBRC).
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafSix">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafSix" aria-expanded="false" aria-controls="collapseTrafSix">
					DURATION
				</span>
			</h2>
		</div>
		<div id="collapseTrafSix" class="collapse" aria-labelledby="headingTrafSix" data-parent="#accordionTraf">
			<div class="card-body">
				<p>
					The duration of this Phase is one (1) month or equivalent to four (4) weeks with 24
					days excluding four (4) Saturdays for a total of 176 training hours.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafSeven">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafSeven" aria-expanded="false" aria-controls="collapseTrafSeven">
					LEARNING AIDS
				</span>
			</h2>
		</div>
		<div id="collapseTrafSeven" class="collapse" aria-labelledby="headingTrafSeven" data-parent="#accordionTraf">
			<div class="card-body">
				<p>
					The following are the materials needed by the PTs for the duration of the Traffic Phase:
					<ul>
						<li> Police Trainee’s Guide </li>
						<li> Other Learning Materials </li>
					</ul>
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafEight">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafEight" aria-expanded="false" aria-controls="collapseTrafEight">
					Module 1 (Orientation and Familiarization. Duties, Responsibilities and Core Values of a Traffic Police Officer )
				</span>
			</h2>
		</div>

		<div id="collapseTrafEight" class="collapse" aria-labelledby="headingTrafEight" data-parent="#accordionTraf">
			<div class="card-body">
				<div id="carousel`TrafMod1" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($TrafMod1Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carousel`TrafMod1" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carousel`TrafMod1" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($TrafMod1Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carousel`TrafMod1" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carousel`TrafMod1" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafMod2">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafMod2" aria-expanded="false" aria-controls="collapseTrafMod2">
					Module 2 (Basic Traffic Laws, Rules and Regulations, Memoranda, Ordinances and Issuances)
				</span>
			</h2>
		</div>

		<div id="collapseTrafMod2" class="collapse" aria-labelledby="headingTrafMod2" data-parent="#accordionTraf">
			<div class="card-body">
				<div id="carouselTrafMod2" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($TrafMod2Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselTrafMod2" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselTrafMod2" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($TrafMod2Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselTrafMod2" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselTrafMod2" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafMod3">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafMod3" aria-expanded="false" aria-controls="collapseTrafMod3">
					Module 3 (Traffic Intersection Conflicts, Signs and Symbols)
				</span>
			</h2>
		</div>

		<div id="collapseTrafMod3" class="collapse" aria-labelledby="headingTrafMod3" data-parent="#accordionTraf">
			<div class="card-body">
				<div id="carouselTrafMod3" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($TrafMod3Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselTrafMod3" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselTrafMod3" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($TrafMod3Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselTrafMod3" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselTrafMod3" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafMod4">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafMod4" aria-expanded="false" aria-controls="collapseTrafMod4">
					Module 4 (Traffic Direction and Control)
				</span>
			</h2>
		</div>

		<div id="collapseTrafMod4" class="collapse" aria-labelledby="headingTrafMod4" data-parent="#accordionTraf">
			<div class="card-body">
				<div id="carouselTrafMod4" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($TrafMod4Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselTrafMod4" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselTrafMod4" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($TrafMod4Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselTrafMod4" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselTrafMod4" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTraf9">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTraf9" aria-expanded="false" aria-controls="collapseTraf9">
					Module 5 (Traffic Law Enforcement)
				</span>
			</h2>
		</div>

		<div id="collapseTraf9" class="collapse" aria-labelledby="headingTraf9" data-parent="#accordionTraf">
			<div class="card-body">
				<div id="carouselTrafMod5" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($TrafMod5Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselTrafMod5" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselTrafMod5" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($TrafMod5Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselTrafMod5" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselTrafMod5" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafMod6">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafMod6" aria-expanded="false" aria-controls="collapseTrafMod6">
					Module 6 (Basic Principles of Defensive Driving)
				</span>
			</h2>
		</div>

		<div id="collapseTrafMod6" class="collapse" aria-labelledby="headingTrafMod6" data-parent="#accordionTraf">
			<div class="card-body">
				<div id="carouselTrafMod6" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($TrafMod6Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselTrafMod6" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselTrafMod6" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($TrafMod6Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselTrafMod6" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselTrafMod6" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTrafMod7">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseTrafMod7" aria-expanded="false" aria-controls="collapseTrafMod7">
					Module 7 (Duties of First Responder in Traffic Accident)
				</span>
			</h2>
		</div>

		<div id="collapseTrafMod7" class="collapse" aria-labelledby="headingTrafMod7" data-parent="#accordionTraf">
			<div class="card-body">
				<div id="carouselTrafMod7" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($TrafMod7Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselTrafMod7" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselTrafMod7" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($TrafMod7Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselTrafMod7" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselTrafMod7" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>