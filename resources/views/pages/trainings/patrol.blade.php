<div class="myaccordion" id="accordionExample">
	<div class="card">
		<div class="card-header" id="headingPatOne">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatOne" aria-expanded="false" aria-controls="collapsePatOne">
					Introduction
				</span>
			</h2>
		</div>

		<div id="collapsePatOne" class="collapse show" aria-labelledby="headingPatOne" data-parent="#accordionExample">
			<div class="card-body">
				<p>Patrol is considered the backbone of police directed activities in the preservation
					of peace and order. Police Officers have the duty to go around a populated area for
					purpose of security and observation. They either move by foot or in motor vehicles in
					crime prone areas within their respective beats to look for and apprehend criminals or
					to respond to citizens under threat or those calling for assistance. The police is ready
					to provide service as summarized in the acronym “SAFE”, that is, the police is Seen,
					Admired, Felt and Experienced. Most often, Police Officers must be visible to the public,
					to let them know that they are ready to help secure the neighborhood. In conducting patrols, the officers intermingle and work with the general populace. The synergy between the police and the community is a primary factor in effective crime
					protection.
					Police Trainees (PTs) under their respective Field Training Officers (FTOs) shall
					engage in beat patrol operations for a period of two (2) months. Working on-the-job
					in one (1) shift lasting for eight (8) hours a day and for six (6) days a week, each PT
					should log a total of 320 manhours doing patrol operations. PTs should not conduct
					patrol operations without the live supervision of their FTO.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatTwo">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatTwo" aria-expanded="false" aria-controls="collapsePatTwo">
					PHASE INFORMATION
				</span>
			</h2>
		</div>
		<div id="collapsePatTwo" class="collapse" aria-labelledby="headingPatTwo" data-parent="#accordionExample">
			<div class="card-body">
				<p>
					The following eight (8) weeks in the Field Training Program (FTP) comprise the
					Patrol Phase having five (5) modules.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatThree">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatThree" aria-expanded="false" aria-controls="collapsePatThree">
					TRAINING GOAL
				</span>
			</h2>
		</div>
		<div id="collapsePatThree" class="collapse" aria-labelledby="headingPatThree" data-parent="#accordionExample">
			<div class="card-body">
				<p>
					The goal of this phase is to introduce the PTs to the fundamental knowledge in
					patrol as one of the law enforcement functions of the Philippine National Police (PNP).
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatFive">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatFIve" aria-expanded="false" aria-controls="collapsePatFIve">
					TRAINING OBJECTIVES
				</span>
			</h2>
		</div>
		<div id="collapsePatFIve" class="collapse" aria-labelledby="headingPatFive" data-parent="#accordionExample">
			<div class="card-body">
				<p>
					At the end of this phase, a PT is expected to perform the following tasks with a high
					degree of efficiency and expertise:
					<br> &MediumSpace;
					a. Develop his patrol skills required as beat patrol officer;<br> &MediumSpace;
					b. Demonstrate proper and correct procedure in the conduct of foot or mobile
					patrol operations; <br> &MediumSpace;
					c. Display proper usage of radio codes and radio operations; <br> &MediumSpace;
					d. Use appropriate arrest techniques, hand-to-hand combat, and weapon skills
					in every operation; and <br> &MediumSpace;
					e. Apply his knowledge on Philippine laws, rules and regulations in every activity.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatFour">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatFour" aria-expanded="false" aria-controls="collapsePatFour">
					TRAINING Audience
				</span>
			</h2>
		</div>
		<div id="collapsePatFour" class="collapse" aria-labelledby="headingPatFour" data-parent="#accordionExample">
			<div class="card-body">
				<p>
					The target audience for this phase are the Police Trainees who completed the
					(6) months Public Safety Basic Recruit Course (PSBRC).
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatSix">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatSix" aria-expanded="false" aria-controls="collapsePatSix">
					DURATION
				</span>
			</h2>
		</div>
		<div id="collapsePatSix" class="collapse" aria-labelledby="headingPatSix" data-parent="#accordionExample">
			<div class="card-body">
				<p>
					The duration of this phase is two months (2), equivalent to eight weeks (8) com-
					prised of 40 days with a total of 320 training hours.
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatSeven">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatSeven" aria-expanded="false" aria-controls="collapsePatSeven">
					LEARNING AIDS
				</span>
			</h2>
		</div>
		<div id="collapsePatSeven" class="collapse" aria-labelledby="headingPatSeven" data-parent="#accordionExample">
			<div class="card-body">
				<p>
					The following are the learning aids required for the duration of the phase:
					<ul>
						<li> Multi-Media Projector </li>
						<li> Multi-Media Pointer </li>
						<li> Laptop Computer </li>
						<li> Projector Screen </li>
						<li> Voice Recorder</li>
						<li> Flip Charts and markers </li>
						<li> Manila Paper </li>
						<li> Extra Sheets of Paper </li>
					</ul>
				</p>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatEight">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatEight" aria-expanded="false" aria-controls="collapsePatEight">
					Module 1 (Orientation and Familiarization; Basic Duties and Responsibilities of a Patrol Officer.)
				</span>
			</h2>
		</div>

		<div id="collapsePatEight" class="collapse" aria-labelledby="headingPatEight" data-parent="#accordionExample">
			<div class="card-body">
				<div id="carouselPatMod1" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselPatMod1" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselPatMod1" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselPatMod1" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselPatMod1" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatMod2">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatMod2" aria-expanded="false" aria-controls="collapsePatMod2">
					Module 2 (Patrol)
				</span>
			</h2>
		</div>

		<div id="collapsePatMod2" class="collapse" aria-labelledby="headingPatMod2" data-parent="#accordionExample">
			<div class="card-body">
				<div id="carouselPatMod2" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($PatMod2Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselPatMod2" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselPatMod2" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($PatMod2Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselPatMod2" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselPatMod2" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatMod3">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatMod3" aria-expanded="false" aria-controls="collapsePatMod3">
					Module 3 (Basic Laws and PNP Regulations)
				</span>
			</h2>
		</div>

		<div id="collapsePatMod3" class="collapse" aria-labelledby="headingPatMod3" data-parent="#accordionExample">
			<div class="card-body">
				<div id="carouselPatMod3" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($PatMod3Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselPatMod3" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselPatMod3" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($PatMod3Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselPatMod3" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselPatMod3" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingPatMod4">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatMod4" aria-expanded="false" aria-controls="collapsePatMod4">
					Module 4 (Police Assistance)
				</span>
			</h2>
		</div>

		<div id="collapsePatMod4" class="collapse" aria-labelledby="headingPatMod4" data-parent="#accordionExample">
			<div class="card-body">
				<div id="carouselPatMod4" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($PatMod4Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselPatMod4" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselPatMod4" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($PatMod4Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselPatMod4" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselPatMod4" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header" id="headingPatMod5">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapsePatMod5" aria-expanded="false" aria-controls="collapsePatMod5">
					Module 5 (Police Community Relation)
				</span>
			</h2>
		</div>

		<div id="collapsePatMod5" class="collapse" aria-labelledby="headingPatMod5" data-parent="#accordionExample">
			<div class="card-body">
				<div id="carouselPatMod5" class="carousel slide" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
						@foreach($PatMod5Images as $key=>$image)
						@if($loop->first)
						<li data-target="#carouselPatMod5" data-slide-to="$key" class="active"></li>
						@else
						<li data-target="#carouselPatMod5" data-slide-to="$key"></li>
						@endif
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($PatMod5Images as $key=>$image)
						@if($loop->first)
						<div class="carousel-item active">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@else
						<div class="carousel-item">
							<img class="d-block w-100" src="{{ asset( $image->path ) }}" alt="{{ asset( $image->path ) }}">
						</div>
						@endif

						@endforeach
					</div>
					<a class="carousel-control-prev" href="#carouselPatMod5" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselPatMod5" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingAssessment">
			<h2 class="mb-0">
				<span class="label collapsed" data-toggle="collapse" data-target="#collapseAssessment" aria-expanded="false" aria-controls="collapseAssessment">
					<a target="_blank" href="http://vls-exam.rstu3.com">Take Exam</a>
				</span>
			</h2>
		</div>

	</div>

</div>