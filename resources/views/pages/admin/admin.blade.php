@extends('layouts.app')
@section('content')
<div class="contianer-fluid">
    <div class="wrapper d-flex align-items-stretch">
        
        <nav id="sidebar" class="active">
            <ul class="list-unstyled components mb-5">
                <li class="active">
                    <a href="#"><span class="fa fa-desktop"></span> Trainings</a>
                </li>
                <li>
                    <a href="#"><span class="fa fa-user"></span> Users</a>
                </li>
            </ul>
        </nav>
        <div id="content" class="p-4 p-md-5">
        
            <h2 class="mb-4">Sidebar #07</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
    </div>
    <script src="{{ asset('/js/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/js/popper.js') }}"></script>
    <script src="{{ asset('/js/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/js/main.js') }}"></script>
</div>
@stop