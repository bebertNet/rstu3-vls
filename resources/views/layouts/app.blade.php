<!doctype html>
<html>

<head>
    @include('partials.head')
</head>

<body>
    <div class="container-fluid">

        <header class="row">
            @include('partials.header')
        </header>

        <div class="main">

            @yield('content')

        </div>
        <br>

        @include('partials.footer')

    </div>
</body>

</html>