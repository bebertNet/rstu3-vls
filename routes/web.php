<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/home', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/courses', function () {
        return view('pages/courses');
    });

    Route::get('/about', function () {
        return view('pages/about');
    });

    Route::get('/courses/FTP', function () {
        $images = DB::table('moduleimages')
            ->where('submodule_id', 1)
            ->get();
        $PatMod2Images = DB::table('moduleimages')
            ->where('submodule_id', 44)
            ->get();
        $PatMod3Images = DB::table('moduleimages')
            ->where('submodule_id', 45)
            ->get();
        $PatMod4Images = DB::table('moduleimages')
            ->where('submodule_id', 46)
            ->get();
        $InvMod1Images = DB::table('moduleimages')
            ->where('submodule_id', 2)
            ->get();
        $InvMod2Images = DB::table('moduleimages')
            ->where('submodule_id', 48)
            ->get();
        $InvMod3Images = DB::table('moduleimages')
            ->where('submodule_id', 49)
            ->get();
        $PatMod5Images = DB::table('moduleimages')
            ->where('submodule_id', 47)
            ->get();
        $mod4Images = DB::table('moduleimages')
            ->where('submodule_id', 50)
            ->get();
        $mod5Images = DB::table('moduleimages')
            ->where('submodule_id', 51)
            ->get();
        $InvMod6Images = DB::table('moduleimages')
            ->where('submodule_id', 52)
            ->get();
        $InvMod7Images = DB::table('moduleimages')
            ->where('submodule_id', 53)
            ->get();
        $TrafMod1Images = DB::table('moduleimages')
            ->where('submodule_id', 3)
            ->get();
        $TrafMod2Images = DB::table('moduleimages')
            ->where('submodule_id', 54)
            ->get();
        $TrafMod3Images = DB::table('moduleimages')
            ->where('submodule_id', 55)
            ->get();
        $TrafMod4Images = DB::table('moduleimages')
            ->where('submodule_id', 56)
            ->get();
        $TrafMod5Images = DB::table('moduleimages')
            ->where('submodule_id', 57)
            ->get();
        $TrafMod6Images = DB::table('moduleimages')
            ->where('submodule_id', 58)
            ->get();
        $TrafMod7Images = DB::table('moduleimages')
            ->where('submodule_id', 59)
            ->get();
        return view('pages/trainings/ftp_pnco',
            ['images'=>$images,
            'mod4Images'=>$mod4Images,
            'mod5Images'=>$mod5Images,
            'PatMod2Images'=>$PatMod2Images,
            'PatMod3Images'=>$PatMod3Images,
            'PatMod4Images'=>$PatMod4Images,
            'PatMod5Images'=>$PatMod5Images,
            'InvMod1Images'=>$InvMod1Images,
            'InvMod2Images'=>$InvMod2Images,
            'InvMod3Images'=>$InvMod3Images,
            'InvMod6Images'=>$InvMod6Images,
            'InvMod7Images'=>$InvMod7Images,
            'TrafMod1Images'=>$TrafMod1Images,
            'TrafMod2Images'=>$TrafMod2Images,
            'TrafMod3Images'=>$TrafMod3Images,
            'TrafMod4Images'=>$TrafMod4Images,
            'TrafMod5Images'=>$TrafMod5Images,
            'TrafMod6Images'=>$TrafMod6Images,
            'TrafMod7Images'=>$TrafMod7Images]
            );
    });

    Route::get('/courses/FTOC', function () {
        $PobcImages = DB::table('moduleimages')
            ->whereIn('submodule_id', [4,5,6,7,8,9,
            10, 11, 12, 13, 14,
            15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25, 26,
            27, 28, 29, 30, 31, 32, 33, 34, 35,
            36, 37, 38, 39, 40, 41, 42, 43])
            ->get();
        $PreImages = DB::table('moduleimages')
            ->where('submodule_id', 21)
            ->get();

        $Modules = DB::table('module')
            ->where('CourseID', 3)
            ->get();
        $SubModules = DB::table('vw_submodule')
            ->where('CourseID', 3)
            ->get();

        return view('pages/trainings/ftoc',
            ['Modules'=>$Modules,
            'PobcImages'=>$PobcImages,
            'PreImages'=>$PreImages,
            'SubModules'=>$SubModules]
            );
    });
  // encoded BY JOSEPH V
  Route::get('/courses/BISOC', function () {
      $BisocImages = DB::table('moduleimages')
          ->whereIn('submodule_id', [  107,108,109,110,111,112,
            113, 114, 115, 116, 117,
            118, 119, 120, 121, 122,
            123, 124, 125, 126, 127, 128, 129,
            130, 131, 132, 133, 134, 135, 136, 137, 138,
            139, 140, 141, 142, 143, 144, 145
        ])
          ->get();
      $Modules = DB::table('module')
          ->where('CourseID', 1)
          ->get();
      $SubModules = DB::table('vw_submodule')
          ->where('CourseID', 1)
          ->get();
      $List = [];

      return view('pages/trainings/bisoc',
          ['Modules'=>$Modules,
          'BisocImages'=>$BisocImages,
          'SubModules'=>$SubModules,
          'List'=>$List]
          );
  });

//end of encoding  from joseph
    Route::get('/courses/POBC', function () {
        $PobcImages = DB::table('moduleimages')
            ->whereIn('submodule_id', [60, 61, 62, 63, 64, 65,
                66, 67, 68, 69, 70,
                71, 72, 73, 74, 75,
                76, 77, 78,
                79, 80, 81, 82, 83, 84,
                85, 86, 87,
                88, 89, 90, 91, 92,
                93, 94, 95, 96,
                97, 98,
                99, 100, 101, 102])
            ->get();
        $Modules = DB::table('module')
            ->where('CourseID', 2)
            ->get();
        $SubModules = DB::table('vw_submodule')
            ->where('CourseID', 2)
            ->get();
        $List = [];

        return view('pages/trainings/pobc',
                    ['Modules'=>$Modules,
                    'PobcImages'=>$PobcImages,
                    'SubModules'=>$SubModules,
                    'List'=>$List]
                    );
    });

    //INTEL
    Route::get('/courses/INTEL', function () {
        $INTELImages = DB::table('moduleimages')
            ->whereIn('submodule_id', [103, 104, 105, 106])
            ->get();
        $Modules = DB::table('module')
            ->where('CourseID', 9)
            ->get();
        $SubModules = DB::table('vw_submodule')
            ->where('CourseID', 9)
            ->get();

        return view('pages/trainings/intel',
            ['Modules'=>$Modules,
            'PobcImages'=>$INTELImages,
            'SubModules'=>$SubModules]
            );
    });

    Route::get('/courses/ASRC', function () {
        $ASRCImages = DB::table('moduleimages')
            ->whereIn('submodule_id', [146, 147, 148, 149,
                150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 161, 162, 163,
                164, 165, 166, 167])
            ->get();
        $Modules = DB::table('module')
            ->where('CourseID', 10)
            ->get();
        $SubModules = DB::table('vw_submodule')
            ->where('CourseID', 10)
            ->get();

        return view('pages/trainings/asrc',
            ['Modules'=>$Modules,
            'ASRCImages'=>$ASRCImages,
            'SubModules'=>$SubModules]
            );
    });

    Route::get('/courses/PASC', function () {
        $PASCImages = DB::table('moduleimages')
            ->whereIn('submodule_id', [168, 169, 170, 171,
                172, 173, 174, 175, 176, 177, 178, 179, 180, 181,
                182, 183, 184, 185, 186, 187, 188, 189, 190, 191,
                192, 193, 194, 195])
            ->get();
        $Modules = DB::table('module')
            ->where('CourseID', 11)
            ->get();
        $SubModules = DB::table('vw_submodule')
            ->where('CourseID', 11)
            ->get();

        return view('pages/trainings/pasc',
            ['Modules'=>$Modules,
            'PASCImages'=>$PASCImages,
            'SubModules'=>$SubModules]
            );
    });



    Route::get('/contactUs', function () {
        return view('pages/contact');
    });

    Route::get('/fileGallery', function () {
        $files = DB::table('file')->get();
        return view('pages/file', ['files'=>$files]);
    });
    Route::get('/admin', function () {
        return view('pages/admin/admin');
    });
});
